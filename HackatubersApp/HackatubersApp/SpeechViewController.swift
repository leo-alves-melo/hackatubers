
//
//  SpeechViewController.swift
//  HackatubersApp
//
//  Created by Juliana Daikawa on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Speech

class SpeechViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    
    let speech = SpeechManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recordButton.isEnabled = false
        speech.speechRecognizer?.delegate = self
        
        // Ask for user's authorization
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
            case .denied:
                isButtonEnabled = false
            case .restricted:
                isButtonEnabled = false
            case .notDetermined:
                isButtonEnabled = false
            }
            
            OperationQueue.main.addOperation() {
                self.recordButton.isEnabled = isButtonEnabled
            }
            
        }
        
        
    }
    
    // NOTE: Speech recognition only lasts about a minute at a time.
    @IBAction func recordButtonPressed(_ sender: Any) {
        
        if speech.audioEngineIsRunning() {
            recordButton.isEnabled = false
            recordButton.setTitle("Start Recording", for: .normal)
            textLabel.text = speech.stop()
        } else {
            speech.start()
            recordButton.setTitle("Stop Recording", for: .normal)
        }
        
        
    }
    
    
    
}

//
//  PatientServices.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

protocol PatientServicesDelegate {
    func didRecieve(patient: Patient)
    
}

class PatientServices {
    
    var delegate: PatientServicesDelegate?
    
    private var dataBaseService: DataBaseService
    
    init(delegate: PatientServicesDelegate) {
        self.delegate = delegate
        self.dataBaseService = DataBaseService.instance
    }
    
    func retrievePatient(ID: String) {
        self.dataBaseService.patientReference.queryOrdered(byChild: "ID").queryEqual(toValue: ID).observeSingleEvent(of: .value) { (snapshot) in
            
            var patient = Patient()
            
            if var data = snapshot.value as? [String: AnyObject] {
                if let data = data.popFirst() {
                     patient = self.parsePatient(data: data)
                }
            }
            
            self.delegate?.didRecieve(patient: patient)
        }
    }
    
    func parsePatient(data: (key: String, value: AnyObject)) -> Patient {
        var patient = Patient()
        
        if let ID = data.value["ID"] as? String {
            patient.ID = ID
        }
        if let name = data.value["name"] as? String {
            patient.name = name
        }
        if let imagePath = data.value["imagePath"] as? String {
            patient.imagePath = imagePath
        }
        if let health = data.value["health"] as? [String: AnyObject] {
            patient.health = self.parseHealth(data: health)
        }
        if let latitude = data.value["latitude"] as? Double {
            patient.latitude = latitude
        }
        if let longitude = data.value["longitude"] as? Double {
            patient.longitude = longitude
        }
        
        return patient
    }
    
    func parseHealth(data: [String: AnyObject]) -> Health {
        var health = Health()
        
        if let biologicalSex = data["biologicalSex"] as? String {
            health.biologicalSex = biologicalSex
        }
        if let bloodType = data["bloodType"] as? String {
            health.bloodType = bloodType
        }
        if let alergies = data["alergies"] as? String {
            health.alergies = alergies
        }
        if let medicaments = data["medicaments"] as? String {
            health.medicaments = medicaments
        }
        if let reactions = data["reactions"] as? String {
            health.reactions = reactions
        }
        if let lastConsultants = data["lastConsultants"] as? String {
            health.lastConsultants = lastConsultants
        }
        if let dateOfBirth = data["dateOfBirth"] as? String {
            health.dateOfBirth = dateOfBirth
        }
        if let userAge = data["userAge"] as? Int {
            health.userAge = userAge
        }
        if let weight = data["weight"] as? Double {
            health.weight = weight
        }
        if let height = data["height"] as? Double {
            health.height = height
        }
        if let caloriesList = data["caloriesList"] as? [[String: AnyObject]] {
            
            var values: [Double] = []
            var days: [String] = []
            
            for calorie in caloriesList {
                values.append(calorie["value"] as! Double)
                days.append(calorie["day"] as! String)
            }
            
            health.caloriesList = (values, days)
        }
        
        if let sleepList = data["sleepList"] as? [[String: AnyObject]] {
            
            var values: [Float] = []
            var days: [String] = []
            
            for sleep in sleepList {
                values.append(Float(sleep["value"] as! Double))
                days.append(sleep["day"] as! String)
            }
            
            health.sleepList = (values, days)
        }
        
        return health
    }
    
    func parse(patient: Patient) -> [String: Any] {
        var json: Dictionary = [String: Any]()
        
        if let name = patient.name {
            json["name"] = name
        }
        if let ID = patient.ID {
            json["ID"] = ID
        }
        if let imagePath = patient.imagePath {
            json["imagePath"] = imagePath
        }
        if let latitude = patient.latitude {
            json["latitude"] = latitude
        }
        if let health = patient.health {
            
            var healthJson: Dictionary = [String: Any]()
            
            if let biologicalSex = health.biologicalSex {
                healthJson["biologicalSex"] = biologicalSex
            }
            if let bloodType = health.bloodType {
                healthJson["bloodType"] = bloodType
            }
            if let dateOfBirth = health.dateOfBirth {
                healthJson["dateOfBirth"] = dateOfBirth
            }
            if let userAge = health.userAge {
                healthJson["userAge"] = userAge
            }
            if let weight = health.weight {
                healthJson["weight"] = weight
            }
            if let height = health.height {
                healthJson["height"] = height
            }
            if let alergies = health.alergies {
                healthJson["alergies"] = alergies
            }
            if let medicaments = health.medicaments {
                healthJson["medicaments"] = medicaments
            }
            if let reactions = health.reactions {
                healthJson["reactions"] = reactions
            }
            if let lastConsultants = health.lastConsultants {
                healthJson["lastConsultants"] = lastConsultants
            }
            
            if let caloriesList = health.caloriesList {
                
                if let values = caloriesList.0 {
                    
                    var array = [[String: Any]]()
                    for index in 0..<values.count {
                        
                        var jsonCalorie: Dictionary = [String: Any]()
                        
                        jsonCalorie["value"] = caloriesList.0![index]
                        jsonCalorie["day"] = caloriesList.1![index]
                        
                        array.append(jsonCalorie)
                    }
                    
                    healthJson["caloriesList"] = array
                }
                
               
            }
            
            if let sleepList = health.sleepList {
                
                if let values = sleepList.0 {
                    var array = [[String: Any]]()
                    for index in 0..<values.count {
                        
                        var jsonSleep: Dictionary = [String: Any]()
                        
                        jsonSleep["value"] = sleepList.0![index]
                        jsonSleep["day"] = sleepList.1![index]
                        
                        array.append(jsonSleep)
                    }
                    
                    healthJson["sleepList"] = array
                }
            }
            
            
            
            
            
            
            json["health"] = healthJson
        }
        
        
        
        
        
        return json
    }
    
    func sendPatient(patient: Patient) {
        
        let json = parse(patient: patient)
        
        dataBaseService.uploadJson(json: json, reference: dataBaseService.patientReference.child(patient.ID!))
    }
}

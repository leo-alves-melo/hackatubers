//
//  DiseaseServices.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

protocol DiseaseServicesDelegate {
    func didRecieve(diseases: [Disease])
    
}

class DiseaseServices {
    var delegate: DiseaseServicesDelegate?
    
    private var dataBaseService: DataBaseService
    
    init(delegate: DiseaseServicesDelegate) {
        self.delegate = delegate
        self.dataBaseService = DataBaseService.instance
    }
    
    func retrieveAllDisease() {
        self.dataBaseService.diseaseReference.observeSingleEvent(of: .value) { (snapshot) in
            
            var diseasesList: [Disease] = []
            
            if let data = snapshot.value as? [String: AnyObject] {
                
                for diseaseData in data {
                    diseasesList.append(self.parseDisease(data: diseaseData))
                }
            }
            
            self.delegate?.didRecieve(diseases: diseasesList)
        }
    }
    
    func parseDisease(data: (key: String, value: AnyObject)) -> Disease {
        var disease = Disease()

        if let name = data.value["name"] as? String {
            disease.name = name
        }
        if let ID = data.value["ID"] as? String {
            disease.ID = ID
        }
        if let diagnostics = data.value["diagnostics"] as? String {
            disease.diagnostics = diagnostics
        }
        if let latitude = data.value["latitude"] as? Double {
            disease.latitude = latitude
        }
        if let longitude = data.value["longitude"] as? Double {
            disease.longitude = longitude
        }
        
        return disease
    }
    
    func sendDisease(disease: Disease) {
        let json = parse(disease: disease)
        
        dataBaseService.uploadJson(json: json, reference: dataBaseService.diseaseReference.child(disease.ID!))
    }
    
    func parse(disease: Disease) -> [String: Any] {
        var json: Dictionary = [String: Any]()
        
        json["name"] = disease.name!
        json["ID"] = disease.ID!
        json["diagnostics"] = disease.diagnostics!
        json["latitude"] = disease.latitude!
        json["longitude"] = disease.longitude!
        
        
        return json
    }
}

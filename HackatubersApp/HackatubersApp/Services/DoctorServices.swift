//
//  DoctorServices.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

@objc protocol DoctorServicesDelegate {
    
}

class DoctorServices {
    weak var delegate: DoctorServicesDelegate?
    
    private var dataBaseService: DataBaseService
    
    init(delegate: DoctorServicesDelegate) {
        self.delegate = delegate
        self.dataBaseService = DataBaseService.instance
    }
    
    func retrieveDoctor() {
        
    }
}

extension DoctorServices: UploadJsonDelegate {
    func jsonUploadDidEnded(_ error: Error?) {
        
    }
    
    func jsonUploadDidEndedPending(_ error: Error?) {
        
    }
    
    
}

//
//  DatabaseServices.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import Firebase

protocol LoginDelegate: class {
    func loginDidEnded(_ error: Error?)
}

protocol CreateUserDelegate: class {
    func userCreationDidEnded(_ error: Error?)
}

protocol UploadImageDelegate: class {
    func imageUploadDidFinish(_ error: Error?)
}

protocol UploadJsonDelegate: class {
    func jsonUploadDidEnded(_ error: Error?)
    func jsonUploadDidEndedPending(_ error: Error?)
}

class DataBaseService {
    let urlStorage = "gs://hackatubers.appspot.com"
    var storageReference: StorageReference?
    
    let patientReference = Database.database().reference(fromURL: "https://hackatubers.firebaseio.com/Patient")
    let doctorReference = Database.database().reference(fromURL: "https://hackatubers.firebaseio.com/Doctor")
    let diseaseReference = Database.database().reference(fromURL: "https://hackatubers.firebaseio.com/Disease")
    
    var userID: Firebase.User?
    static let instance = DataBaseService()
    
    weak var loginDelegate: LoginDelegate?
    weak var createUserDelegate: CreateUserDelegate?
    weak var uploadImageDelegate: UploadImageDelegate?
    weak var uploadJsonDelegate: UploadJsonDelegate?
    
    private var logged: Bool = false
    
    private init() {
        let storage = Storage.storage()
        self.storageReference = storage.reference(forURL: self.urlStorage)
        Auth.auth().addStateDidChangeListener { [weak self] (_, user) in
            if user != nil {
                self?.logged = true
            } else {
                self?.login(email: "leonardo.alves.melo.1995@gmail.com", password: "ornitocoders")
                self?.logged = false
            }
        }
    }
    
    func login(email: String, password: String) {

        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                self.loginDelegate?.loginDidEnded(error)
            } else {
                print("Login Success")
                self.userID = user?.user
                self.logged = true
                self.loginDelegate?.loginDidEnded(error)
            }
        }
    }
    
    func createUser(email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                print("Sucess! User created!!")
                self.userID = user?.user
            }
            self.createUserDelegate?.userCreationDidEnded(error)
        })
    }
    
    func uploadJson(json: [String: Any], reference: DatabaseReference) {
        reference.setValue(json) { (error, _) in
            if let error = error {
                print("Erro ao dar upload de Json: \(error)")
            }
            DataBaseService.instance.uploadJsonDelegate?.jsonUploadDidEnded(error)
        }
    }
    
    func uploadJsonPending(json: [String: AnyObject], reference: DatabaseReference) {
        reference.setValue(json) { (error, _) in
            if let error = error {
                print("Erro ao dar upload de Json: \(error)")
            }
            DataBaseService.instance.uploadJsonDelegate?.jsonUploadDidEndedPending(error)
        }
    }
    func getStorageReference(path: String) -> StorageReference {
        
        return self.storageReference!.child(path)
    }
    func getImage(path: String, completion: @escaping (Data) -> Void) {
        
        let imageRef = self.getStorageReference(path: path)
        imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print("LOG from ImageServices.getImageFromDatabase: \(error.localizedDescription)")
            } else {
                OperationQueue.main.addOperation({
                    
                    completion(data!)
                })
            }
        }
        
    }
    
}

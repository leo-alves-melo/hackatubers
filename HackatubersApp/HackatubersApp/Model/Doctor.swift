//
//  Doctor.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Doctor {
    var name: String?
    var crm: Int?
    var url: String?
}

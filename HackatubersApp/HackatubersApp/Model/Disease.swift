//
//  Disease.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Disease {
    var name: String?
    var diagnostics: String?
    var ID: String?
    var latitude: Double?
    var longitude: Double?
}

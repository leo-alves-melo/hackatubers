//
//  MyAnnotation.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit
import UIKit

class MyAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var id: String
    var image: UIImage
    
    init(coordinate: CLLocationCoordinate2D, id: String, image: UIImage) {
        self.coordinate = coordinate
        self.id = id
        self.image = image
        
        super.init()
    }
}

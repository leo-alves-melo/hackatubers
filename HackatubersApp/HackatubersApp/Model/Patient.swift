//
//  Patient.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Patient {
    var name: String?
    var health: Health?
    var ID: String?
    var imagePath: String?
    var latitude: Double?
    var longitude: Double?
}

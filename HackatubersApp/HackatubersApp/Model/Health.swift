//
//  Health.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Health {
    var biologicalSex: String?
    var bloodType: String?
    var dateOfBirth: String?
    var userAge: Int?
    var caloriesList: ([Double]?, [String]?)?
    var sleepList: ([Float]?, [String]?)?
    var weight: Double?
    var height: Double?
    var alergies: String?
    var medicaments: String?
    var reactions: String?
    var lastConsultants: String?
}

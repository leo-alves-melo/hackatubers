//
//  AppointmentViewController.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class AppointmentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let manager = ShortcutManager(viewController: self)
        manager.instatiateShortcuts()

        // Do any additional setup after loading the view.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AppointmentViewController: ShortcutManagerDelegate {
    func applyShortcut() {
        let alert = UIAlertController(title: "Hi There!", message: "Hey there! Glad to see you got this working!", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

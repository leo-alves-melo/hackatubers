//
//  DiseaseMap.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import MapKit

class DiseaseMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var purpleView: UIView!
    
    var diseasesList: [Disease] = []
    var diseaseListShow: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.redView.layer.masksToBounds = true
        self.redView.layer.cornerRadius = 4
        
        self.purpleView.layer.masksToBounds = true
        self.purpleView.layer.cornerRadius = 4
        
        let location = CLLocationCoordinate2D(latitude: -22.90, longitude:  -47.063)
        
        let viewRegion = MKCoordinateRegion(center: location, latitudinalMeters: 1400, longitudinalMeters: 1400)
        mapView.setRegion(viewRegion, animated: false)
        
        self.addDiseases()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        diseasesList = []
        
        //self.diseaseTableView.reloadData()
        
       
        
        let diseaseServices = DiseaseServices(delegate: self)
        diseaseServices.retrieveAllDisease()
        
    }
    
    func addDiseases() {
        for disease in self.diseasesList {
            //let annotation = MKPointAnnotation()
            
            if disease.name == "Dengue" {
                let annotation = MyAnnotation(coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0), id: "Dengue", image: UIImage(named: "pin1")!)
                annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(disease.latitude!), longitude: CLLocationDegrees(disease.longitude!))
                annotation.id = disease.name!
                
                self.mapView.addAnnotation(annotation)
            }
            else {
                let annotation = MyAnnotation(coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0), id: "Caxumba", image: UIImage(named: "pin2")!)
                annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(disease.latitude!), longitude: CLLocationDegrees(disease.longitude!))
                annotation.id = disease.name!
                
                self.mapView.addAnnotation(annotation)
            }
            
        }
    }
}

extension DiseaseMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var diseaseAnnotation: AnnotationView?

        if let annotation = annotation as? MyAnnotation {
            diseaseAnnotation = AnnotationView(annotation: annotation, reuseIdentifier: annotation.id)
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 43, height: 57))
            imageView.image =  annotation.image
            diseaseAnnotation?.addSubview(imageView)

            self.mapView.sendSubviewToBack(diseaseAnnotation!)
        }

        diseaseAnnotation?.frame.size.height = 40
        diseaseAnnotation?.frame.size.width = 40

        return diseaseAnnotation
        
        
        
        
//        let annotationIdentifier = "AnnotationIdentifier"
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
//
//        if annotationView == nil {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
//            annotationView!.canShowCallout = true
//        }
//        else {
//            annotationView!.annotation = annotation
//        }
//
//        let pinImage = UIImage(named: "pin")
//        annotationView!.image = pinImage
//
//        return annotationView
        
//        let view = MKAnnotationView()
//        view.image = UIImage(named: "pin")
//
//        return view
        
    }
    
}

extension DiseaseMapViewController: DiseaseServicesDelegate {
    func didRecieve(diseases: [Disease]) {
        self.diseasesList = diseases
        
        DispatchQueue.main.async {
            self.addDiseases()
        }
        
    }
    
    
}

//
//  FindDiseaseViewController.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class FindDiseaseViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var diseaseTableView: UITableView!
    
    var diseasesList: [Disease] = []
    var diseaseListShow: [String] = []
    var selectedDiseases: [Disease] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.diseaseTableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        diseasesList = []
        diseaseListShow = []
        selectedDiseases = []
        
        self.diseaseTableView.reloadData()
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let diseaseServices = DiseaseServices(delegate: self)
        diseaseServices.retrieveAllDisease()
        
    }
    
    func prepareDiseaseListShow() {
        for disease in diseasesList {
            if !diseaseListShow.contains(disease.name!) {
                diseaseListShow.append(disease.name!)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DiseaseMapViewController {
            vc.diseasesList = self.selectedDiseases
        }
    }
}

extension FindDiseaseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.diseaseListShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiseaseCell", for: indexPath) as! DiseaseCellTableViewCell
        
        cell.diseaseTitle.text = diseaseListShow[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedDiseases = self.diseasesList.filter({ (disease) -> Bool in
            disease.name == self.diseaseListShow[indexPath.row]
        })
        
        self.performSegue(withIdentifier: "SelectedDisease", sender: nil)
    }
    
    
}

extension FindDiseaseViewController: DiseaseServicesDelegate {
    func didRecieve(diseases: [Disease]) {
        self.diseasesList = diseases
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.prepareDiseaseListShow()
        self.diseaseTableView.reloadData()
    }
    
    
}

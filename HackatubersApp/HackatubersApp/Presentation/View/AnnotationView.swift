//
//  AnnotationView.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit

class AnnotationView: MKAnnotationView {
    /// Override the layer factory for this class to return a custom CALayer class
    override class var layerClass: AnyClass {
        return ZPositionableLayer.self
    }
    
    /// convenience accessor for setting zPosition
    var stickyZPosition: CGFloat {
        get {
            return (self.layer as! ZPositionableLayer).stickyZPosition
        }
        set {
            (self.layer as! ZPositionableLayer).stickyZPosition = newValue
        }
    }
    
    /// force the pin to the front of the z-ordering in the map view
    func bringViewToFront() {
        superview?.bringSubviewToFront(self)
        stickyZPosition = CGFloat(1)
    }
    
    /// force the pin to the back of the z-ordering in the map view
    func setViewToDefaultZOrder() {
        stickyZPosition = CGFloat(0)
    }
    
}

private class ZPositionableLayer: CALayer {

    override var zPosition: CGFloat {
        get {
            return super.zPosition
        }
        set {
        }
    }

    var stickyZPosition: CGFloat {
        get {
            return super.zPosition
        }
        set {
            super.zPosition = newValue
        }
    }
}

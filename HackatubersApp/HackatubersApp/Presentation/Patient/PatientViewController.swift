//
//  PatientViewController.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit

class PatientViewController: UIViewController {
    
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var textConsult: UITextView!
    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var basicBarChart: BasicBarChart!
    @IBOutlet weak var sonoChart: BasicBarChart!
    
    @IBOutlet weak var bodyImage: UIImageView!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var bloodTypeLabel: UILabel!
    
    @IBOutlet weak var alergyLabel: UILabel!
    
    @IBOutlet weak var medications: UILabel!
    @IBOutlet weak var reactionsLabel: UILabel!
    @IBOutlet weak var lastConsultantDataLabel: UILabel!
    
    var patientID: String!
    var patient: Patient!
    var speechManager = SpeechManager()
    var startedSpeech = false
    
    var sleepTime: [Int] = []
    var calories: [Int] = []
    
    var disease = Disease()
    
    @IBOutlet weak var userSex: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppUtility.lockOrientation(.landscapeLeft, andRotateTo: .landscapeLeft)
        
        self.patientImage.layer.masksToBounds = true
        self.patientImage.layer.cornerRadius = self.patientImage.frame.width/2
        
        self.patientImage.backgroundColor = UIColor.white
        
        let patientServices = PatientServices(delegate: self)
        patientServices.retrievePatient(ID: patientID)
        
        self.textConsult.layer.borderWidth = 1
        self.textConsult.layer.borderColor = UIColor(red:226/255, green:226/255, blue:226/255, alpha: 1).cgColor
        self.textConsult.layer.cornerRadius = 3
        self.textConsult.layer.masksToBounds = true

        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    func updatePatientData() {
        self.patientName.text = patient.name!
        self.ageLabel.text = "\(patient.health!.userAge!)"
        self.weightLabel.text = "\(Int(patient.health!.weight!))Kg"
        self.heightLabel.text = "\(patient.health!.height!)m"
        self.userSex.text = patient.health!.biologicalSex
        self.bloodTypeLabel.text = patient.health!.bloodType
        self.alergyLabel.text = patient.health!.alergies
        self.medications.text = patient.health!.medicaments
        self.reactionsLabel.text = patient.health!.reactions
        self.lastConsultantDataLabel.text = patient.health!.lastConsultants
        
        if patient.health?.biologicalSex == "Masculino" {
            self.bodyImage.image = UIImage(named: "silhueta-men")
        } else {
            self.bodyImage.image = UIImage(named: "silhueta-fem")
        }
        
        self.updateCaloriesChart()
        self.updateSleepChart()
    }
    
    func updateSleepChart() {
        var entries: [BarEntry] = []
        
        for index in 0..<patient.health!.sleepList!.0!.count {
            entries.append(BarEntry(color: UIColor(red: 231.0/255.0, green: 127.0/255.0, blue: 2.0/255.0, alpha: 1.0), height: Float(patient.health!.sleepList!.0![index])/8.0, textValue: "\(patient.health!.sleepList!.0![index])h", title: patient.health!.sleepList!.1![index]))
        }
        
        self.basicBarChart.dataEntries = entries
    }
    
    func updateCaloriesChart() {
        
        var entries: [BarEntry] = []
        
        for index in 0..<patient.health!.caloriesList!.0!.count {
            entries.append(BarEntry(color: UIColor(red: 102.0/255.0, green: 82.0/255.0, blue: 246.0/255.0, alpha: 1.0), height: Float(patient.health!.caloriesList!.0![index])/1200.0, textValue: "\(patient.health!.caloriesList!.0![index])cal", title: patient.health!.caloriesList!.1![index]))
        }
        
        self.sonoChart.dataEntries = entries
    }
    
    @IBOutlet weak var startButton: UIButton!
    
    @IBAction func changeSpeechState(_ sender: Any) {
        if !startedSpeech {
            startedSpeech = true
            self.speechManager.start()
            startButton.setTitle("Parar Consulta", for: .normal)
        }
        if startButton.titleLabel?.text == "Parar Consulta" {
            let speechString = self.speechManager.stop()!
            self.filterSpeech(speech: speechString)
            startButton.setTitle("Finalizar", for: .normal)
            
        }
        if startButton.titleLabel?.text == "Finalizar" {
            self.disease.diagnostics = self.textConsult.text
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            self.patient.health?.lastConsultants = dateFormatter.string(from: Date())
            self.finish()
        }
    }
    
    func finish() {
        let diseaseServices = DiseaseServices(delegate: self)
        diseaseServices.sendDisease(disease: disease)
        
        let patienteServices = PatientServices(delegate: self)
        patienteServices.sendPatient(patient: patient)
        _ = navigationController?.popViewController(animated: true)
    }
    
    func filterSpeech(speech: String) {
        
        let words = speech.components(separatedBy: " ")
        
        var consultantText = ""
        
        for word in words {
            if word == "malária" {
                consultantText.append("O paciente apresentou sintomas de malária, que vão desde:\n- Febre alta\n- Dores no corpo")
                self.disease.name = "Malária"
                self.disease.ID = "malaria"
                self.disease.latitude = patient.latitude
                self.disease.longitude = patient.longitude
            }
        }
        
        self.textConsult.text = consultantText
    }
    
}

extension PatientViewController: PatientServicesDelegate {
    func didRecieve(patient: Patient) {
        self.patient = patient
        
        DispatchQueue.main.async {
            self.updatePatientData()
            DataBaseService.instance.getImage(path: patient.imagePath!, completion:  { (data) in
                
                DispatchQueue.main.async {
                    self.patientImage.image = UIImage(data: data)
                    self.activity.isHidden = true
                }
            })
        }
    }
    
    
}

extension PatientViewController: DiseaseServicesDelegate {
    func didRecieve(diseases: [Disease]) {
        
    }
    
    
}

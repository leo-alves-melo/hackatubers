//
//  ScannerQRCodeViewController.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 25/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerQRCodeViewController: UIViewController {
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    var patientID: String = ""
    
    @IBOutlet weak var qrCodeFrameView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        AVCaptureDevice.requestAccess(for: .video) { (response) in
            
            do {
                let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
                
               
                let input: AnyObject! = try AVCaptureDeviceInput(device: captureDevice!)
                
                
                // Initialize the captureSession object.
                self.captureSession = AVCaptureSession()
                // Set the input device on the capture session.
                self.captureSession?.addInput(input as! AVCaptureInput)
                
                // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
                let captureMetadataOutput = AVCaptureMetadataOutput()
                self.captureSession?.addOutput(captureMetadataOutput)
                
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
                
                self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
                self.videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                
                DispatchQueue.main.async {
                    self.videoPreviewLayer?.frame = self.qrCodeFrameView.layer.bounds
                    self.qrCodeFrameView.layer.addSublayer(self.videoPreviewLayer!)
                    
                    self.captureSession?.startRunning()
                }
                
            }
            catch {
                
            }
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         self.tabBarController?.tabBar.isHidden = false
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PatientViewController {
            vc.patientID = self.patientID
        }
    }
    

}

extension ScannerQRCodeViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if self.patientID != "" {
            return
        }
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {

            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {

            if metadataObj.stringValue != nil {
                self.patientID = metadataObj.stringValue!
                
                
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "Patient", sender: nil)
                }
                
            }
        }
    }
}

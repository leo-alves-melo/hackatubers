//
//  HealthKitManager.swift
//  HackatubersApp
//
//  Created by Juliana Daikawa on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import HealthKit

class HealthKitManager {
    
    private let store = HKHealthStore()
    
    func isHealthKitAvailable() -> Bool {
        if HKHealthStore.isHealthDataAvailable() {
            return true
        } else {
            return false
        }
    }
    
    func getUsersAuthorization() {
        
        // Objects to write and read
        guard let dateOfBirth = HKObjectType.characteristicType(forIdentifier: .dateOfBirth),
            let bloodType = HKObjectType.characteristicType(forIdentifier: .bloodType),
            let biologicalSex = HKObjectType.characteristicType(forIdentifier: .biologicalSex),
            let bodyMassIndex = HKObjectType.quantityType(forIdentifier: .bodyMassIndex),
            let height = HKObjectType.quantityType(forIdentifier: .height),
            let bodyMass = HKObjectType.quantityType(forIdentifier: .bodyMass),
            let allergies = HKObjectType.clinicalType(forIdentifier: .allergyRecord),
            let condition = HKObjectType.clinicalType(forIdentifier: .conditionRecord),
            let medication = HKObjectType.clinicalType(forIdentifier: .medicationRecord),
            let sleep = HKObjectType.categoryType(forIdentifier: .sleepAnalysis),
            let activeEnergy = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned) else {
                return
        }
        
        let writableTypes: Set<HKSampleType> = [bodyMassIndex,
                                                activeEnergy,
                                                height,
                                                HKObjectType.workoutType()]
        
        let readableTypes: Set<HKObjectType> = [dateOfBirth,
                                                bloodType,
                                                biologicalSex,
                                                bodyMassIndex,
                                                height,
                                                bodyMass,
                                                allergies,
                                                condition,
                                                medication,
                                                sleep,
                                                activeEnergy,
                                                HKObjectType.workoutType()]
        
        // Asking for user's authorization
        store.requestAuthorization(toShare: writableTypes, read: readableTypes) { (success, error) in
            print(success)
        }
        
    }
    
    // Biological sex
    func getUserBiologicalSex() -> String? {
        
        do {
            
            let biologicalSex = try store.biologicalSex()
            
            switch biologicalSex.biologicalSex {
            case .female:
                return "Female"
            case .male:
                return "Male"
            case .other:
                return "Other"
            case .notSet:
                return "Not informed"
            }
            
        } catch {
            return "Not informed"
        }
        
    }
    
    // Blood type
    func getUserBloodType() -> String? {
        
        do {
            
            let bloodType = try store.bloodType()
            
            switch bloodType.bloodType {
            case .abNegative:
                return "AB-"
            case .abPositive:
                return "AB+"
            case .aNegative:
                return "A-"
            case .aPositive:
                return "A+"
            case .bNegative:
                return "B-"
            case .bPositive:
                return "B+"
            case .oNegative:
                return "O-"
            case .oPositive:
                return "O+"
            case .notSet:
                return "Not informed"
                
            }
            
        } catch {
            return "Not informed"
        }
        
    }
    
    // Date of birth
    func getUserDateOfBirth() -> Date? {
        
        do {
            
            let dateOfBirth = try store.dateOfBirthComponents()
            return dateOfBirth.date
            
        } catch {
            return nil
        }
        
    }
    
    // Age
    func getUserAge() -> Int? {
        
        do {
            
            let birthdayComponents = try store.dateOfBirthComponents()
            let today = Date()
            let calendar = Calendar.current
            let todayDateComponents = calendar.dateComponents([.year],
                                                              from: today)
            let thisYear = todayDateComponents.year!
            let age = thisYear - birthdayComponents.year!
            
            return age
            
        } catch {
            return nil
        }
        
    }
    
    // Getting user's sleep data
    // 0: Duration
    // 1: Dates
    func retrieveSleepAnalysis(completion: @escaping (_ value: ([Float]?, [String]?)) -> Void) {
        
        var array = [Float]()
        var dateArray = [String]()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        if let sleepType = HKObjectType.categoryType(forIdentifier: .sleepAnalysis) {
            
            // Use a sortDescriptor to get the recent data first
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            
            // we create our query with a block completion to execute
            let query = HKSampleQuery(sampleType: sleepType, predicate: nil, limit: 30, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                
                if error != nil {
                    return
                }
                
                if let result = tmpResult {
                    
                    // do something with my data
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            if sample.value == HKCategoryValueSleepAnalysis.asleep.rawValue {
                                
                                let duration = self.stringFromTime(interval: sample.endDate.timeIntervalSince(sample.startDate))
                                
                                array.append(duration)
                                dateArray.append(formatter.string(from: sample.endDate))
                                
                            }
                            
                        }
                    }
                    
                    completion((array, dateArray))
                    
                }
            }
            
            store.execute(query)
            
        } else {
            completion((nil, nil))
        }
    }
    
    
    // Getting user's energy burned data
    // 0: Calories
    // 1: Dates
    func retrieveCaloriesBurned(completion: @escaping (_ value: ([Double]?, [String]?)) -> Void) {
        
        var array = [Double]()
        var dateArray = [String]()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let type = HKSampleType.quantityType(forIdentifier: .activeEnergyBurned)!
        let query = HKSampleQuery(sampleType: type, predicate: nil, limit: 30, sortDescriptors: nil) { (query, results, error) in
            
            if let result = results {
                for item in result {
                    
                    if let sample = item as? HKQuantitySample {
                        
                        let calorie = sample.quantity.doubleValue(for: .calorie())
                        
                        array.append(calorie)
                        dateArray.append(formatter.string(from: sample.endDate))
                    }
                }
                completion((array, dateArray))
            } else {
                completion((nil, nil))
            }
            
        }
        
        self.store.execute(query)
    }
    
    // Time interval to hours
    func stringFromTime(interval: TimeInterval) -> Float {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        return Float(formatter.string(from: interval)!)!/60
    }
    
    // Get quantity type information - peso e altura
    func getUserInfo(of identifier: HKQuantityTypeIdentifier, completion: @escaping (_ value: HKQuantity?) -> Void) {
        // Reading information
        let type = HKSampleType.quantityType(forIdentifier: identifier)!
        let query = HKSampleQuery(sampleType: type, predicate: nil, limit: 10, sortDescriptors: nil) { (query, results, error) in
            
            if let result = results?.first as? HKQuantitySample {
                completion(result.quantity)
            } else {
                completion(nil)
            }
            
        }
        
        self.store.execute(query)
    }
    
    // Get clinical type information - medicação, alergia, condições
    func getUserInfo(of identifier: HKClinicalTypeIdentifier, completion: @escaping (_ value: [HKClinicalRecord]?) -> Void) {
        // Reading height
        let type = HKObjectType.clinicalType(forIdentifier: identifier)!
        let query = HKSampleQuery(sampleType: type, predicate: nil, limit: 1, sortDescriptors: nil) { (query, results, error) in
            
            if let result = results as? [HKClinicalRecord] {
                completion(result)
            } else {
                completion(nil)
            }
            
        }
        
        self.store.execute(query)
    }
    
    // Write quantity type information
    func writeToUserInfo(of identifier: HKQuantityTypeIdentifier, unit: HKUnit, value: Double) {
        
        if let type = HKSampleType.quantityType(forIdentifier: identifier) {
            let date = Date()
            let quantity = HKQuantity(unit: unit, doubleValue: value)
            let sample = HKQuantitySample(type: type, quantity: quantity, start: date, end: date)
            self.store.save(sample, withCompletion: { (success, error) in
                print("Saved \(success), error \(error)")
            })
        }
        
    }
    
}

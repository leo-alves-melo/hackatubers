//
//  ShortcutManager.swift
//  HackatubersApp
//
//  Created by Leonardo Alves de Melo on 24/08/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Intents

protocol ShortcutManagerDelegate {
    func applyShortcut()
}

class ShortcutManager {

// Do any additional setup after loading the view, typically from a nib.

    weak var viewController: UIViewController?
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
        

    func instatiateShortcuts() {
        let activity = NSUserActivity(activityType: "com.leonardo.HackatubersApp.disease")
        activity.title = "Disease"
        activity.userInfo = ["speech": "disease"]
        activity.isEligibleForSearch = true
        activity.isEligibleForPrediction = true
        activity.persistentIdentifier = NSUserActivityPersistentIdentifier("com.leonardo.HackatubersApp.disease")

        self.viewController?.view.userActivity = activity
        activity.becomeCurrent()
    }

    

//@IBAction func botao(_ sender: Any) {
//INPreferences.requestSiriAuthorization { (status) in
//print("Status: \(status): =? \(status == INSiriAuthorizationStatus.authorized)")
//
//
//}
//}
}



